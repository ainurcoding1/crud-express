import { v4 as uuidv4 } from 'uuid';
import faker from 'faker';
import connection from './config/database';

async function insertUsers() {
  try {
    const usersData = Array.from({ length: 10 }, () => ({
      id: uuidv4(),
      roles: Math.floor(Math.random() * 3) + 1,
      name: faker.name.findName(),
      email: faker.internet.email(),
      password: faker.internet.password(),
    }));

    await connection('users').insert(usersData);

    console.log('Data inserted successfully');
  } catch (error) {
    console.error('Failed to insert data:', error);
  } finally {
    await connection.destroy();
  }
}

insertUsers();