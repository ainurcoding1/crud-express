import knex from 'knex';
import config from '../config/knexfile';

const environment: keyof typeof config = process.env.NODE_ENV as keyof typeof config || 'development';
const connection = knex(config[environment]);

export default connection;