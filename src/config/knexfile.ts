// src/config/knexfile.ts
import { Knex } from 'knex';

const config = {
  development: {
    client: 'mysql2',
    connection: {
      host: 'localhost',
      user: 'root',
      password: '',
      database: 'ainur_test',
    },
    migrations: {
      directory: __dirname + '/../database/migrations',
    },
    seeds: {
      directory: __dirname + '/../database/seeds',
    },
  },
};

export default config;