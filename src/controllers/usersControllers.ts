import { Request, Response } from "express";
import {getUsers} from '../models/usersModel';

export async function getUsersController(req: Request, res: Response): Promise<void> {
  try {
    const users = await getUsers();
    res.json(users);
  } catch (e) {
    res.status(500).json({ error: 'Failed to get users. ' +  e });
  }
}