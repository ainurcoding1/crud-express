import express, { Request, Response, NextFunction} from 'express';
import connection from './database/database';
import v1 from './routes/v1/v1Routes';

const app = express();

// routes
app.use(v1);

// Middleware untuk menangani kesalahan 404 (Not Found)
app.use((req: Request, res: Response, next: NextFunction) => {
  res.status(404).json({ error: 'Not Found' });
});

// Middleware untuk menangani kesalahan server internal
app.use((err: any, req: Request, res: Response, next: NextFunction) => {
  console.error(err);
  res.status(500).json({ error: 'Internal Server Error' });
});

app.listen(3000, () => {
  console.log('Server is running on port 3000');
});

