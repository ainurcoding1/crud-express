import express from 'express';
import * as todoUsers from '../../../controllers/usersControllers';

const router = express.Router();

router.get('/list', todoUsers.getUsersController);

export default router;