import express from 'express';
import usersRoutes from './users/usersRoutes';

const router = express.Router();


router
  .use('/v1/users', usersRoutes);

export default router;