import connection from '../database/database';
// interfaces
import { users } from '../interfaces/users/usersInterface';

const conn = connection;

export async function getUsers(): Promise<users[]> {
  return conn('users').select('*');
}



